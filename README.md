# Content Watched Event Producer

Streaming Pipeline to convert raw VideoStreamEvent produced by the Video Streaming Platform when a video stream Starts and Stops, into
ContentWatchedEvent.

## Requirements

### Definitions
   * Contents are streaming video data, played by a client.
   * A streaming session is a way of identifying and tracing an authenticated user.

### Input

#### Video Stream Events

Every time a customer plays a video stream on the online platform, a Start event is produced, followed by a Stop event when they finish watching the content.
PubSub `VideoStreamEvents` topic in Avro format, with:
* eventTimestamp: OffsetDateTime when the event was generated 
* sessionId: String session identifier 
* event being either
    * Start(userId: String, contentId: String) stream started 
    * Stop stream stopped
    
### Output

#### Content Watched Events
PubSub `ContentWatchedEvents` topic in Avro format, with: 
* startTimestamp: OffsetDateTime timestamp of the Start event
* userId: String
* contentId: String
* timeWatched: Duration

### Rules
* When a Stop event is received, publish a Content Watched Event if the duration is longer than 10m. 
* Video Stream Events can come out of order. Stop events coming 24h after the related Start events should be discarded.

## Technical Details 

### Framework and Library Details
* Avro
* Google Cloud PubSub
* Apache Beam
* Direct Runner
* JDK 8

## Build And Execution

* Use `mvn clean install` command on terminal. This will create Avro classes required and are used by the ContentWatchedEventProcessor.

* The following configuration is required to execute the project
    1. GOOGLE_APPLICATION_CREDENTIALS needs to be configured and set as and environment variable.
    2. Topic Subscription details of video-stream-event and set as PipelineOptions`subscription`: `projects/<project-id>/subscriptions/<subscription-name>`
    3. New Topic needs to be created and set as PipelineOptions `topic`: `projects/<project-id>/topic/<topic-name>`
    
* Tests are JUnit tests and can be executed as any other JUnit tests.


## Planned Improvements

### Implementation

* There is a failing test case because which is set to be ignored. This is because Iterator doesn't preserve the order

* Fine tune the window and trigger and watermarking config and add more tests cases around it

* Need to add Date Format to make the Date parsing a bit more strict

* Code can be refactoring to make code more clean

* Move auto generated avro classes to target directory

* Running the project on JDK version 11 or 12 is having the following runtime issue. Need to be addressed.

```
org.apache.beam.vendor.guava.v20_0.com.google.common.util.concurrent.UncheckedExecutionException: java.lang.UnsupportedOperationException: Cannot define class using reflection: Cannot define nest member class java.lang.reflect.AccessibleObject$Cache + within different package then class org.apache.beam.repackaged.core.net.bytebuddy.mirror.AccessibleObject
        at org.apache.beam.vendor.guava.v20_0.com.google.common.cache.LocalCache$Segment.get(LocalCache.java:2214)
        at org.apache.beam.vendor.guava.v20_0.com.google.common.cache.LocalCache.get(LocalCache.java:4053)
        at org.apache.beam.vendor.guava.v20_0.com.google.common.cache.LocalCache.getOrLoad(LocalCache.java:4057)
        at org.apache.beam.vendor.guava.v20_0.com.google.common.cache.LocalCache$LocalLoadingCache.get(LocalCache.java:4986)
        at org.apache.beam.runners.direct.DoFnLifecycleManager.get(DoFnLifecycleManager.java:61)
        at org.apache.beam.runners.direct.ParDoEvaluatorFactory.createEvaluator(ParDoEvaluatorFactory.java:127)
        at org.apache.beam.runners.direct.ParDoEvaluatorFactory.forApplication(ParDoEvaluatorFactory.java:79)
        at org.apache.beam.runners.direct.TransformEvaluatorRegistry.forApplication(TransformEvaluatorRegistry.java:169)
        at org.apache.beam.runners.direct.DirectTransformExecutor.run(DirectTransformExecutor.java:117)
        at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:515)
```

### Deployment

* The application is tested using DirectRunner. But it needs to be thoroughly tested again based on the chosen runner.

* Perform some load testing on the runner once the runner of choice is decided

