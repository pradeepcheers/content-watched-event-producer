package com.sky.sdp;

import com.sky.sdp.avro.ContentWatchedEvent;
import com.sky.sdp.avro.VideoStreamEvent;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.windowing.AfterPane;
import org.apache.beam.sdk.transforms.windowing.AfterWatermark;
import org.apache.beam.sdk.transforms.windowing.Sessions;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import static com.sky.sdp.TestHelper.buildContentWatchedEvents;
import static com.sky.sdp.TestHelper.buildVideoStreamEventGroupByKey;

public class ContentWatchedEventProcessorTest implements Serializable {

    private static final List<VideoStreamEvent> videoStreamEventsList = TestHelper.generateVideoSteamEvents();

    @Rule
    public final transient TestPipeline pipeline = TestPipeline.create();

    @Test
    public void shouldTestVideoStreamEventsKVMapping() {
        PCollection<VideoStreamEvent> videoStreamEvents = pipeline.apply(Create.of(videoStreamEventsList));

        PCollection<KV<String, VideoStreamEvent>> keyValCollection = videoStreamEvents
                .apply("Create KV pair with sessionId as key",
                        MapElements.via(new SimpleFunction<VideoStreamEvent, KV<String, VideoStreamEvent>>() {
            @Override
            public KV<String, VideoStreamEvent> apply(VideoStreamEvent videoStreamEvent) {
                return KV.of(videoStreamEvent.getSessionId().toString(), videoStreamEvent);
            }
        }));

        List<KV<String, VideoStreamEvent>> output = videoStreamEventsList
                .stream()
                .map(e -> KV.of(e.getSessionId().toString(), e))
                .collect(Collectors.toList());

        PAssert.that(keyValCollection).containsInAnyOrder(output.toArray(new KV[]{}));

        pipeline.run();
    }

    @Test
    @Ignore // TODO: The test is intermittently fail due to `Iterable<VideoStreamEvent>>` doesn't persist order.
    public void shouldTestVideoStreamEventsGroupByKeyMapping() {
        List<KV<String, VideoStreamEvent>> kvVideoStreamEventsList = videoStreamEventsList
                .stream()
                .map(e -> KV.of(e.getSessionId().toString(), e))
                .collect(Collectors.toList());

        PCollection<KV<String, VideoStreamEvent>> kvVideoStreamEvents = pipeline.apply(Create.of(kvVideoStreamEventsList));

        PCollection<KV<String, Iterable<VideoStreamEvent>>> groupedCollection = kvVideoStreamEvents
                .apply("Apply Session Window and Trigger config",
                        Window.<KV<String, VideoStreamEvent>>into(Sessions.withGapDuration(Duration.standardMinutes(10)))
                        .triggering(AfterWatermark.pastEndOfWindow().withLateFirings(AfterPane.elementCountAtLeast(2)))
                        .withAllowedLateness(Duration.standardHours(24)).accumulatingFiredPanes())

                .apply("Group By Key", GroupByKey.create());

        PAssert.that(groupedCollection).containsInAnyOrder(buildVideoStreamEventGroupByKey());

        pipeline.run();
    }

    @Test
    public void shouldTestContentWatchedEventTransformer() {
        PCollection<KV<String, Iterable<VideoStreamEvent>>> groupByKeyCollection = pipeline
                .apply(Create.of(buildVideoStreamEventGroupByKey()));

        PCollection<ContentWatchedEvent> contentWatchedEventKVPair =
                groupByKeyCollection.apply("Validate and generate ContentWatchedEvents",
                        ParDo.of(new ContentWatchedEventProcessor.ContentWatchedEventTransformer()));

        PAssert.that(contentWatchedEventKVPair).containsInAnyOrder(buildContentWatchedEvents());

        pipeline.run();
    }

    @Test
    public void shouldFilterOutEvents_MinCriteria() {
        List<ContentWatchedEvent> input = buildContentWatchedEvents().stream()
                .peek(c -> {
                    if (c.getUserId().toString().equalsIgnoreCase("u1") ||
                            c.getUserId().toString().equalsIgnoreCase("u2"))
                        c.setTimeWatched(java.time.Duration.ofMinutes(4).toString());
                }).collect(Collectors.toList());

        PCollection<ContentWatchedEvent> contentWatchedEvents = pipeline.apply(Create.of(input));

        PCollection<ContentWatchedEvent> filteredMinDurationCriteria = contentWatchedEvents
                .apply("Filter Min Duration Criteria",
                Filter.by((ContentWatchedEvent contentWatchedEvent) ->
                        java.time.Duration.parse(contentWatchedEvent.getTimeWatched()).abs().toMinutes() > 10));

        List<ContentWatchedEvent> contentWatchedEventsFiltered = input
                .stream()
                .filter(c -> !c.getUserId().toString().equalsIgnoreCase("u1") &&
                        !c.getUserId().toString().equalsIgnoreCase("u2"))
                .collect(Collectors.toList());

        PAssert.that(filteredMinDurationCriteria).containsInAnyOrder(contentWatchedEventsFiltered);

        pipeline.run();
    }

    @Test
    public void shouldFilterOutEvents_MaxCriteria() {
        List<ContentWatchedEvent> input = buildContentWatchedEvents().stream()
                .peek(c -> {
                    if (c.getUserId().toString().equalsIgnoreCase("u1") ||
                            c.getUserId().toString().equalsIgnoreCase("u2"))
                        c.setTimeWatched(java.time.Duration.ofDays(1).toString());
                }).collect(Collectors.toList());

        PCollection<ContentWatchedEvent> contentWatchedEvents = pipeline.apply(Create.of(input));

        PCollection<ContentWatchedEvent> filteredMinDurationCriteria = contentWatchedEvents
                .apply("Filter Min Duration Criteria",
                    Filter.by((ContentWatchedEvent contentWatchedEvent) ->
                        java.time.Duration.parse(contentWatchedEvent.getTimeWatched()).abs().toHours() < 24));

        List<ContentWatchedEvent> contentWatchedEventsFiltered = input
                .stream()
                .filter(c -> !c.getUserId().toString().equalsIgnoreCase("u1") &&
                        !c.getUserId().toString().equalsIgnoreCase("u2"))
                .collect(Collectors.toList());

        PAssert.that(filteredMinDurationCriteria).containsInAnyOrder(contentWatchedEventsFiltered);

        pipeline.run();
    }
}
