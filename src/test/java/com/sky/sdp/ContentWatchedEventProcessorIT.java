package com.sky.sdp;

import com.google.common.collect.Lists;
import com.sky.sdp.avro.ContentWatchedEvent;
import com.sky.sdp.avro.VideoStreamEvent;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.testing.TestStream;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.windowing.AfterPane;
import org.apache.beam.sdk.transforms.windowing.AfterWatermark;
import org.apache.beam.sdk.transforms.windowing.IntervalWindow;
import org.apache.beam.sdk.transforms.windowing.Sessions;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.junit.Rule;
import org.junit.Test;

import java.io.Serializable;
import java.util.ArrayList;

import static com.sky.sdp.TestHelper.buildContentWatchedEvent;
import static com.sky.sdp.TestHelper.buildVideoStreamEvent;

public class ContentWatchedEventProcessorIT implements Serializable {

    @Rule
    public final transient TestPipeline pipeline = TestPipeline.create();

    @Test
    public void shouldTestPipelineBehavior_eventArrival() {
        Instant baseTime = new Instant(0);

        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true),
                        buildVideoStreamEvent("1", "2019-01-01T13:00:00.30+01:00", false))
                .addElements(buildVideoStreamEvent("2", "2019-01-01T10:00:00.30+01:00", true),
                        buildVideoStreamEvent("2", "2019-01-01T13:00:00.30+01:00", false))
                .advanceWatermarkTo(baseTime.plus(Duration.standardMinutes(1)))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        ContentWatchedEvent contentWatchedEvent1 =
                buildContentWatchedEvent("1", "2019-01-01T10:00:00.300+01:00", "2019-01-01T13:00:00.30+01:00");
        ContentWatchedEvent contentWatchedEvent2 =
                buildContentWatchedEvent("2", "2019-01-01T10:00:00.300+01:00", "2019-01-01T13:00:00.30+01:00");

        ArrayList<ContentWatchedEvent> expected = Lists.newArrayList(contentWatchedEvent1, contentWatchedEvent2);

        PAssert.that(output).containsInAnyOrder(expected);

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineWaterMarkBehavior_withAdvancedWaterMarkToTenMinutes() {
        Instant baseTime = new Instant(0);

        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true),
                        buildVideoStreamEvent("1", "2019-01-01T13:00:00.30+01:00", false))
                .advanceWatermarkTo(baseTime)
                .addElements(buildVideoStreamEvent("2", "2019-01-01T10:00:00.30+01:00", true),
                        buildVideoStreamEvent("2", "2019-01-01T13:00:00.30+01:00", false))
                .advanceWatermarkTo(baseTime.plus(Duration.standardMinutes(10)))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        ContentWatchedEvent contentWatchedEvent1 =
                buildContentWatchedEvent("1", "2019-01-01T10:00:00.300+01:00", "2019-01-01T13:00:00.30+01:00");
        ContentWatchedEvent contentWatchedEvent2 =
                buildContentWatchedEvent("2", "2019-01-01T10:00:00.300+01:00", "2019-01-01T13:00:00.30+01:00");

        ArrayList<ContentWatchedEvent> expected = Lists.newArrayList(contentWatchedEvent1, contentWatchedEvent2);

        PAssert.that(output)
                .inOnTimePane(new IntervalWindow(baseTime, Duration.standardMinutes(1)))
                .inWindow(new IntervalWindow(baseTime, Duration.standardMinutes(1)))
                .containsInAnyOrder(Lists.newArrayList(contentWatchedEvent2));

        PAssert.that(output).containsInAnyOrder(expected);

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_withAdvancedWaterMarkAboveMaxThreshold() {
        Instant baseTime = new Instant(0);

        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true))
                .advanceWatermarkTo(baseTime.plus(Duration.standardHours(25)))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T13:00:00.30+01:00", false))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        PAssert.that(output).containsInAnyOrder(Lists.newArrayList());

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_whenThereAreNoEvents() {
        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        PAssert.that(output).containsInAnyOrder(Lists.newArrayList());

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_whenThereIsNoStopEvent() {
        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        PAssert.that(output).containsInAnyOrder(Lists.newArrayList());

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_whenTheWatchedEventDurationIsLessThanMinThreshold() {
        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:10:00.30+01:00", false))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);


        PAssert.that(output).containsInAnyOrder(Lists.newArrayList());

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_whenTheWatchedEventDurationIsGreaterThanMinThreshold() {
        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:11:00.30+01:00", false))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        ContentWatchedEvent contentWatchedEvent =
                buildContentWatchedEvent("1", "2019-01-01T10:00:00.300+01:00", "2019-01-01T10:11:00.30+01:00");

        PAssert.that(output).containsInAnyOrder(Lists.newArrayList(contentWatchedEvent));

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_whenTheWatchedEventDurationIsLessThanMaxThreshold() {
        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T12:10:00.30+01:00", false))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        ContentWatchedEvent contentWatchedEvent =
                buildContentWatchedEvent("1", "2019-01-01T10:00:00.300+01:00", "2019-01-01T12:10:00.30+01:00");

        PAssert.that(output).containsInAnyOrder(Lists.newArrayList(contentWatchedEvent));

        pipeline.run();
    }

    @Test
    public void shouldTestPipelineBehavior_whenTheWatchedEventDurationIsGreaterThanMaxThreshold() {
        TestStream<VideoStreamEvent> videoStreamEventTestStream = TestStream.create(AvroCoder.of(VideoStreamEvent.class))
                .addElements(buildVideoStreamEvent("1", "2019-01-01T10:00:00.30+01:00", true))
                .addElements(buildVideoStreamEvent("1", "2019-01-02T10:11:00.30+01:00", false))
                .advanceWatermarkToInfinity();

        PCollection<ContentWatchedEvent> output = triggerPipeline(videoStreamEventTestStream);

        PAssert.that(output).containsInAnyOrder(Lists.newArrayList());

        pipeline.run();
    }

    private PCollection<ContentWatchedEvent> triggerPipeline(TestStream<VideoStreamEvent> videoStreamEventTestStream) {
        return pipeline.apply(videoStreamEventTestStream)
                .apply("Create KV pair with sessionId as key",
                        MapElements.via(new SimpleFunction<VideoStreamEvent, KV<String, VideoStreamEvent>>() {
                            @Override
                            public KV<String, VideoStreamEvent> apply(VideoStreamEvent videoStreamEvent) {
                                return KV.of(videoStreamEvent.getSessionId().toString(), videoStreamEvent);
                            }
                        }))
                .apply("Apply Session Window and Trigger config",
                        Window.<KV<String, VideoStreamEvent>>into(Sessions.withGapDuration(Duration.standardMinutes(1)))
                                .triggering(AfterWatermark.pastEndOfWindow().withLateFirings(AfterPane.elementCountAtLeast(2)))
                                .withAllowedLateness(Duration.standardHours(24)).discardingFiredPanes())
                .apply("Group by Session Id", GroupByKey.create())
                .apply("Validate and generate ContentWatchedEvents",
                        ParDo.of(new ContentWatchedEventProcessor.ContentWatchedEventTransformer()))
                .apply("Filter out events doesn't meet min threshold duration criteria ",
                        Filter.by((ContentWatchedEvent contentWatchedEvent) ->
                                java.time.Duration.parse(contentWatchedEvent.getTimeWatched()).abs().toMinutes() > 10))
                .apply("Filter out events doesn't max duration criteria",
                        Filter.by((ContentWatchedEvent contentWatchedEvent) ->
                                java.time.Duration.parse(contentWatchedEvent.getTimeWatched()).abs().toHours() < 24));
    }
}
