package com.sky.sdp;

import com.sky.sdp.avro.ContentWatchedEvent;
import com.sky.sdp.avro.Start;
import com.sky.sdp.avro.Stop;
import com.sky.sdp.avro.VideoStreamEvent;
import com.sky.sdp.avro.VideoStreamEventStart;
import com.sky.sdp.avro.VideoStreamEventStop;
import org.apache.beam.sdk.values.KV;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestHelper {

    public static List<VideoStreamEvent> generateVideoSteamEvents() {
        List<VideoStreamEvent> videoStreamEvents = IntStream.rangeClosed(1, 5)
                .mapToObj(id -> buildVideoStreamEvent(String.valueOf(id), "2019-01-01T10:00:00.30+01:00", true))
                .collect(Collectors.toList());

        videoStreamEvents.addAll(IntStream.rangeClosed(1, 5)
                .mapToObj(id -> buildVideoStreamEvent(String.valueOf(id), "2019-01-01T13:00:00.30+01:00", false))
                .collect(Collectors.toList()));

        return videoStreamEvents;
    }

    public static VideoStreamEvent buildVideoStreamEvent(String id, String eventTimestamp, boolean isStartEvent) {
        VideoStreamEvent videoStreamEvent = new VideoStreamEvent();
        videoStreamEvent.setEventTimestamp(eventTimestamp);
        videoStreamEvent.setSessionId("s" + id);

        if (isStartEvent) {
            VideoStreamEventStart videoStreamEventStart = new VideoStreamEventStart();
            videoStreamEventStart.setStart(new Start("u" + id, "c" + id));
            videoStreamEvent.setEvent(videoStreamEventStart);
        } else {
            VideoStreamEventStop videoStreamEventStop = new VideoStreamEventStop();
            videoStreamEventStop.setStop(new Stop());
            videoStreamEvent.setEvent(videoStreamEventStop);
        }

        return videoStreamEvent;
    }

    public static List<KV<String, Iterable<VideoStreamEvent>>> buildVideoStreamEventGroupByKey() {
        Map<CharSequence, List<VideoStreamEvent>> groupByKey = generateVideoSteamEvents()
                .stream()
                .collect(Collectors.groupingBy(VideoStreamEvent::getSessionId));

        Stream<KV<String, Iterable<VideoStreamEvent>>> kvStream =
                groupByKey
                        .entrySet()
                        .stream()
                        .map(e -> KV.of(e.getKey().toString(), e.getValue()));

        return kvStream.collect(Collectors.toList());
    }

    public static List<ContentWatchedEvent> buildContentWatchedEvents() {

        return IntStream.rangeClosed(1, 5)
                .mapToObj(c -> buildContentWatchedEvent(String.valueOf(c),
                        "2019-01-01T10:00:00.300+01:00", "2019-01-01T13:00:00.30+01:00"))
                .collect(Collectors.toList());
    }

    public static ContentWatchedEvent buildContentWatchedEvent(String id, String startTimestamp, String stopTimestamp) {

        ContentWatchedEvent contentWatchedEvent = new ContentWatchedEvent();
        contentWatchedEvent.setStartTimestamp(startTimestamp);
        contentWatchedEvent.setUserId("u" + id);
        contentWatchedEvent.setContentId("c" + id);
        Duration duration = Duration
                .between(getEventUTCTime(stopTimestamp), getEventUTCTime(startTimestamp));
        contentWatchedEvent.setTimeWatched(duration.toString());

        return contentWatchedEvent;
    }

    private static LocalDateTime getEventUTCTime(String eventDateTime) {
        return LocalDateTime.ofInstant(OffsetDateTime.parse(eventDateTime).toInstant(), ZoneOffset.UTC);
    }
}
