package com.sky.sdp;

import com.google.common.collect.Lists;
import com.sky.sdp.avro.ContentWatchedEvent;
import com.sky.sdp.avro.Start;
import com.sky.sdp.avro.VideoStreamEvent;
import com.sky.sdp.avro.VideoStreamEventStart;
import com.sky.sdp.avro.VideoStreamEventStop;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.Validation;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.windowing.AfterPane;
import org.apache.beam.sdk.transforms.windowing.AfterWatermark;
import org.apache.beam.sdk.transforms.windowing.Sessions;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.joda.time.Duration;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class ContentWatchedEventProcessor {

    public interface Options extends PipelineOptions {
        @Description("The Cloud Pub/Sub topic subscription")
        @Default.String("video-stream-event-subscription")
        @Validation.Required
        String getSubscription();

        void setSubscription(String subscription);

        @Description("The Cloud Pub/Sub topic to publish")
        @Default.String("content-watched-event-topic")
        @Validation.Required
        String getTopic();

        void setTopic(String subscription);

        @Description("Minimum duration threshold in minutes to generate Content Watched Event")
        @Default.Integer(10)
        int getMinDurationThreshold();

        void setMinDurationThreshold(int value);

        @Description("Maximum duration threshold in hours to generate Content Watched Event")
        @Default.Integer(24)
        @Validation.Required
        int getMaxDurationThreshold();

        void setMaxDurationThreshold(int value);

        @Description("Video Stream Event count criteria to generate Content Watched Event")
        @Default.Integer(2)
        @Validation.Required
        int getVideoStreamEventCountCriteria();

        void setVideoStreamEventCountCriteria(int value);

        @Description("Minimum window gap duration in minutes")
        @Default.Integer(1)
        @Validation.Required
        int getMinimumWindowGapDuration();

        void setMinimumWindowGapDuration(int value);
    }

    public static void main(String... args) {
        Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
        Pipeline pipeline = Pipeline.create(options);

        PCollection<VideoStreamEvent> avroInput = pipeline
                .apply("Read Avro records from Pub/Sub subscription",
                        PubsubIO.readAvros(VideoStreamEvent.class).fromSubscription(options.getSubscription()));

        PCollection<KV<String, VideoStreamEvent>> keyValCollection = avroInput
                .apply("Create KV pair with sessionId as key", MapElements.via(new SimpleFunction<VideoStreamEvent, KV<String, VideoStreamEvent>>() {
                    @Override
                    public KV<String, VideoStreamEvent> apply(VideoStreamEvent videoStreamEvent) {
                        return KV.of(videoStreamEvent.getSessionId().toString(), videoStreamEvent);
                    }
                }));

        PCollection<KV<String, Iterable<VideoStreamEvent>>> groupedCollection = keyValCollection
                .apply("Apply Session Window and Trigger config",
                        Window.<KV<String, VideoStreamEvent>>into(Sessions.withGapDuration(Duration.standardMinutes(options.getMinimumWindowGapDuration())))
                                .triggering(AfterWatermark.pastEndOfWindow().withLateFirings(AfterPane.elementCountAtLeast(options.getVideoStreamEventCountCriteria())))
                                .withAllowedLateness(Duration.standardHours(options.getMaxDurationThreshold())).discardingFiredPanes())
                .apply("Group by Session Id", GroupByKey.create());

        PCollection<ContentWatchedEvent> contentWatchedEvents =
                groupedCollection.apply("Validate and generate ContentWatchedEvents", ParDo.of(new ContentWatchedEventTransformer()));

        PCollection<ContentWatchedEvent> filteredMinDurationCriteria = contentWatchedEvents
                .apply("Filter out events doesn't meet min threshold duration criteria ",
                        Filter.by((ContentWatchedEvent contentWatchedEvent) ->
                                java.time.Duration.parse(contentWatchedEvent.getTimeWatched()).abs().toMinutes() > options.getMinDurationThreshold()));

        PCollection<ContentWatchedEvent> filteredMaxDurationCriteria = filteredMinDurationCriteria
                .apply("Filter out events doesn't max duration criteria",
                        Filter.by((ContentWatchedEvent contentWatchedEvent) ->
                                java.time.Duration.parse(contentWatchedEvent.getTimeWatched()).abs().toHours() < options.getMaxDurationThreshold()));

        filteredMaxDurationCriteria
                .apply("Publish Content Watched Events",
                        PubsubIO.writeAvros(ContentWatchedEvent.class).to("content-watched-event-topic"));

        pipeline.run().waitUntilFinish();
    }

    static class ContentWatchedEventTransformer extends DoFn<KV<String, Iterable<VideoStreamEvent>>, ContentWatchedEvent> {

        @ProcessElement
        public void processElement(ProcessContext processContext) {

            KV<String, Iterable<VideoStreamEvent>> keyVal = processContext.element();
            String key = keyVal.getKey();
            Iterable<VideoStreamEvent> val = keyVal.getValue();

            if(Lists.newArrayList(val).size() == 2) {
                ContentWatchedEvent contentWatchedEvent = buildContentWatchedEvent(key, val);

                processContext.output(contentWatchedEvent);
            }
        }

        private ContentWatchedEvent buildContentWatchedEvent(String key, Iterable<VideoStreamEvent> val) {

            ContentWatchedEvent contentWatchedEvent = new ContentWatchedEvent();
            LocalDateTime stopTime = null;
            LocalDateTime startTime = null;

            for(VideoStreamEvent v : val) {
                if(v.getSessionId().toString().equals(key) && v.getEvent() instanceof VideoStreamEventStop) {
                    stopTime = LocalDateTime.ofInstant(OffsetDateTime.parse(v.getEventTimestamp().toString()).toInstant(), ZoneOffset.UTC);
                }
                if(v.getSessionId().toString().equals(key) && v.getEvent() instanceof VideoStreamEventStart) {
                    // TODO Add DateFormat
                    OffsetDateTime startOffsetDateTime = OffsetDateTime.parse(v.getEventTimestamp().toString());
                    startTime = LocalDateTime.ofInstant(startOffsetDateTime.toInstant(), ZoneOffset.UTC);

                    Start start = ((VideoStreamEventStart)v.getEvent()).getStart();
                    contentWatchedEvent.setStartTimestamp(startOffsetDateTime.toString());
                    contentWatchedEvent.setContentId(start.getContentId());
                    contentWatchedEvent.setUserId(start.getUserId());
                }
            }
            contentWatchedEvent.setTimeWatched(java.time.Duration.between(stopTime, startTime).toString());

            return contentWatchedEvent;
        }
    }
}
